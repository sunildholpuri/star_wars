package com.example.trojansd.starwars.network;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by trojansd on 27-09-2016.
 */
public class MovieResponse {


    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("height")
    @Expose
    private String height;
    @SerializedName("mass")
    @Expose
    private String mass;
    @SerializedName("hair_color")
    @Expose
    private String hair_color;
    @SerializedName("skin_color")
    @Expose
    private String skin_color;
    @SerializedName("eye_color")
    @Expose
    private String eye_color;
    @SerializedName("birth_year")
    @Expose
    private String birth_year;
    @SerializedName("gender")
    @Expose
    private String gender;
    @SerializedName("homeworld")
    @Expose
    private String homeworld;
    @SerializedName("films")
    @Expose
    private List<String> films = new ArrayList<String>();
    @SerializedName("species")
    @Expose
    private List<String> species = new ArrayList<String>();
    @SerializedName("vehicles")
    @Expose
    private List<String> vehicles = new ArrayList<String>();
    @SerializedName("starships")
    @Expose
    private List<String> starships = new ArrayList<String>();
    @SerializedName("created")
    @Expose
    private String created;
    @SerializedName("edited")
    @Expose
    private String edited;
    @SerializedName("url")
    @Expose
    private String url;

    /**
     *
     * @return
     * The name
     */
    public String getName() {
        return name;
    }

    /**
     *
     * @param name
     * The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     *
     * @return
     * The height
     */
    public String getHeight() {
        return height;
    }

    /**
     *
     * @param height
     * The height
     */
    public void setHeight(String height) {
        this.height = height;
    }

    /**
     *
     * @return
     * The mass
     */
    public String getMass() {
        return mass;
    }

    /**
     *
     * @param mass
     * The mass
     */
    public void setMass(String mass) {
        this.mass = mass;
    }

    /**
     *
     * @return
     * The hair_color
     */
    public String getHair_color() {
        return hair_color;
    }

    /**
     *
     * @param hair_color
     * The hair_color
     */
    public void setHair_color(String hair_color) {
        this.hair_color = hair_color;
    }

    /**
     *
     * @return
     * The skin_color
     */
    public String getSkin_color() {
        return skin_color;
    }

    /**
     *
     * @param skin_color
     * The skin_color
     */
    public void setSkin_color(String skin_color) {
        this.skin_color = skin_color;
    }

    /**
     *
     * @return
     * The eye_color
     */
    public String getEye_color() {
        return eye_color;
    }

    /**
     *
     * @param eye_color
     * The eye_color
     */
    public void setEye_color(String eye_color) {
        this.eye_color = eye_color;
    }

    /**
     *
     * @return
     * The birth_year
     */
    public String getBirth_year() {
        return birth_year;
    }

    /**
     *
     * @param birth_year
     * The birth_year
     */
    public void setBirth_year(String birth_year) {
        this.birth_year = birth_year;
    }

    /**
     *
     * @return
     * The gender
     */
    public String getGender() {
        return gender;
    }

    /**
     *
     * @param gender
     * The gender
     */
    public void setGender(String gender) {
        this.gender = gender;
    }

    /**
     *
     * @return
     * The homeworld
     */
    public String getHomeworld() {
        return homeworld;
    }

    /**
     *
     * @param homeworld
     * The homeworld
     */
    public void setHomeworld(String homeworld) {
        this.homeworld = homeworld;
    }

    /**
     *
     * @return
     * The films
     */
    public List<String> getFilms() {
        return films;
    }

    /**
     *
     * @param films
     * The films
     */
    public void setFilms(List<String> films) {
        this.films = films;
    }

    /**
     *
     * @return
     * The species
     */
    public List<String> getSpecies() {
        return species;
    }

    /**
     *
     * @param species
     * The species
     */
    public void setSpecies(List<String> species) {
        this.species = species;
    }

    /**
     *
     * @return
     * The vehicles
     */
    public List<String> getVehicles() {
        return vehicles;
    }

    /**
     *
     * @param vehicles
     * The vehicles
     */
    public void setVehicles(List<String> vehicles) {
        this.vehicles = vehicles;
    }

    /**
     *
     * @return
     * The starships
     */
    public List<String> getStarships() {
        return starships;
    }

    /**
     *
     * @param starships
     * The starships
     */
    public void setStarships(List<String> starships) {
        this.starships = starships;
    }

    /**
     *
     * @return
     * The created
     */
    public String getCreated() {
        return created;
    }

    /**
     *
     * @param created
     * The created
     */
    public void setCreated(String created) {
        this.created = created;
    }

    /**
     *
     * @return
     * The edited
     */
    public String getEdited() {
        return edited;
    }

    /**
     *
     * @param edited
     * The edited
     */
    public void setEdited(String edited) {
        this.edited = edited;
    }

    /**
     *
     * @return
     * The url
     */
    public String getUrl() {
        return url;
    }

    /**
     *
     * @param url
     * The url
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
