package com.example.trojansd.starwars.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.trojansd.starwars.R;

/**
 * Created by trojansd on 27-09-2016.
 */
public class Starships extends Fragment {
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.movies_fragments, null);
        return view;}
}
