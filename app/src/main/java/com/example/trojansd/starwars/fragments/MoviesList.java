package com.example.trojansd.starwars.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.example.trojansd.starwars.R;
import com.example.trojansd.starwars.activity.MovieEightActivity;
import com.example.trojansd.starwars.activity.MovieFiveActivity;
import com.example.trojansd.starwars.activity.MovieFourActivity;
import com.example.trojansd.starwars.activity.MovieSecondActivity;
import com.example.trojansd.starwars.activity.MovieSevenActivity;
import com.example.trojansd.starwars.activity.MovieSixActivity;
import com.example.trojansd.starwars.activity.MovieThirdActivity;
import com.example.trojansd.starwars.activity.MoviefActivity;

/**
 * Created by Trojansd on 23-09-2016.
 */
public class MoviesList extends Fragment implements View.OnClickListener {

    private LinearLayout firstMovie,secondmovie,thirdmovie,fourmovie,fivemovie,sixmovie,sevenmovie,eightmovie;
    //Overriden method onCreateView
    @Override
    public View onCreateView(final LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.movies_fragments, null);
        firstMovie=(LinearLayout)view.findViewById(R.id.FirstMovie);
        secondmovie=(LinearLayout)view.findViewById(R.id.SecondMovie);
        thirdmovie=(LinearLayout)view.findViewById(R.id.ThirdMovie);
        fourmovie=(LinearLayout)view.findViewById(R.id.FourthMovie);
        fivemovie=(LinearLayout)view.findViewById(R.id.FithMovie);
        sixmovie=(LinearLayout)view.findViewById(R.id.sixthMovie);
        sevenmovie=(LinearLayout)view.findViewById(R.id.SeventhMovie);
        eightmovie=(LinearLayout)view.findViewById(R.id.EightMovie);

        firstMovie.setOnClickListener(this);
        secondmovie.setOnClickListener(this);
        thirdmovie.setOnClickListener(this);
        fourmovie.setOnClickListener(this);
        fivemovie.setOnClickListener(this);
        sixmovie.setOnClickListener(this);
        sevenmovie.setOnClickListener(this);
        eightmovie.setOnClickListener(this);




        return view;
    }

    @Override
    public void onClick(View v) {
        switch(v.getId()){
            case R.id.FirstMovie:
                Intent intent=new Intent(getActivity(), MoviefActivity.class);
                startActivity(intent);
                break;
            case R.id.SecondMovie:
                Intent intent2=new Intent(getActivity(), MovieSecondActivity.class);
                startActivity(intent2);
                break;
            case R.id.ThirdMovie:
                Intent intent3=new Intent(getActivity(), MovieThirdActivity.class);
                startActivity(intent3);
                break;
            case R.id.FourthMovie:
                Intent intent4=new Intent(getActivity(), MovieFourActivity.class);
                startActivity(intent4);
                break;
            case R.id.FithMovie:
                Intent intent5=new Intent(getActivity(), MovieFiveActivity.class);
                startActivity(intent5);
                break;
            case R.id.sixthMovie:
                Intent intent6=new Intent(getActivity(), MovieSixActivity.class);
                startActivity(intent6);
                break;
            case R.id.SeventhMovie:
                Intent intent7=new Intent(getActivity(), MovieSevenActivity.class);
                startActivity(intent7);
                break;
            case R.id.EightMovie:
                Intent intent8=new Intent(getActivity(), MovieEightActivity.class);
                startActivity(intent8);
                break;

        }

    }
}
