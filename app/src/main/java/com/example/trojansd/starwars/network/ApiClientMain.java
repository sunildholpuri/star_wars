package com.example.trojansd.starwars.network;

import com.squareup.okhttp.OkHttpClient;

import java.util.concurrent.TimeUnit;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.POST;


public class ApiClientMain {
    private static StarwarApiInterface StarwarApiInterface;  // interface for Retrofit api
    public static final String URL = "http://swapi.co/api/";
    public static final String MEDIA_TYPE_STRING = "text/plain";
    public static final String MEDIA_TYPE_IMAGE = "image/*";

    public static StarwarApiInterface getApiClient() {

        if (StarwarApiInterface == null) {

            OkHttpClient client = new OkHttpClient();
            client.setConnectTimeout(20, TimeUnit.SECONDS);
            client.setReadTimeout(15, TimeUnit.SECONDS);
            client.setWriteTimeout(15, TimeUnit.SECONDS);
            Retrofit retrofit = new Retrofit.Builder()
                    .baseUrl(URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();

            StarwarApiInterface= retrofit.create(StarwarApiInterface.class);
        }
        return StarwarApiInterface;
    }


    public interface StarwarApiInterface {


@GET("people/1/")
Call<MovieResponse> getMovieResponse();


    }

    public static RequestBody getStringRequestBody(String s) {
        return RequestBody.create(MediaType.parse(MEDIA_TYPE_STRING), s);
    }
}
